module github.com/simon-engledew/linuxkit-hashicorp-vault/go/cmd/console

go 1.13

require (
	github.com/xdsopl/framebuffer v0.0.0-20171019113941-ed6572609f8c
	golang.org/x/image v0.0.0-20200801110659-972c09e46d76
	golang.org/x/sys v0.0.0-20200828161417-c663848e9a16
)
