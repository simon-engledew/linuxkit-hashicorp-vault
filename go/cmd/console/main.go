package main // import "github.com/simon-engledew/linuxkit-hashicorp-vault/go/cmd/console"

import (
	"flag"
	"image"
	"image/color"
	"image/draw"
	_ "image/png"
	"io/ioutil"
	"log"
	"os"

	"github.com/xdsopl/framebuffer/src/framebuffer"
	"golang.org/x/image/font"
	"golang.org/x/image/font/basicfont"
	"golang.org/x/image/math/fixed"
	"golang.org/x/sys/unix"
)

func main() {
	flag.Parse()
	text := flag.Arg(0)
	if text == "" {
		log.Fatal("usage: console TEXT\n")
	}

	fd, err := os.Open("/dev/tty2")
	if err != nil {
		log.Fatalf( "failed to open file descriptor for /dev/tty2: %s\n", err)
	}

	if err := unix.IoctlSetInt(int(fd.Fd()), 0x4B3A, 0x01); err != nil {
		log.Fatalf( "failed to set graphics mode: %s\n", err)
	}

	if err := ioutil.WriteFile("/sys/class/graphics/fbcon/cursor_blink", []byte("0"), 0600); err != nil {
		log.Fatalf("failed to stop cursor blink: %s\n", err)
	}

	fb, err := framebuffer.Open("/dev/fb0")
	if err != nil {
		log.Fatalf( "failed to open /dev/fb0: %s\n", err)
	}

	draw.Draw(fb, fb.Bounds(), &image.Uniform{color.RGBA{255, 255, 255, 255}}, image.ZP, draw.Src)

	mid := fb.Bounds().Size().Div(2).Sub(image.Point{X: len(text) * 7, Y: 13}.Div(2)).Mul(64)

	black := color.RGBA{R: 0, G: 0, B: 0, A: 255}
	point := fixed.Point26_6{
		X: fixed.Int26_6(mid.X),
		Y: fixed.Int26_6(mid.Y),
	}

	d := &font.Drawer{
		Dst:  fb,
		Src:  image.NewUniform(black),
		Face: basicfont.Face7x13,
		Dot:  point,
	}
	d.DrawString(text)
}
