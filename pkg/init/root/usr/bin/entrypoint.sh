#!/bin/sh

set -e

ADDR="$(addr eth0)"

if [ ! -f /var/disk/certs/https.pem ]; then
    openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
        -subj "/CN=${ADDR}" \
        -addext "subjectAltName = IP:${ADDR}" \
        -keyout /var/disk/certs/https.key  -out /var/disk/certs/https.crt

    (umask 0077; cat /var/disk/certs/https.key /var/disk/certs/https.crt > /var/disk/certs/https.pem)
fi

chvt 2

echo "Loading console..."

exec /usr/bin/console "Connect to https://${ADDR}"
